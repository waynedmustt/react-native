import { StyleSheet, Dimensions } from 'react-native';

const screen = Dimensions.get("window");
const buttonWidth = screen.width;
const buttonHeight = screen.height / 4;

export default StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#202020',
      justifyContent: 'flex-end'
    },
    buttonContainer: {
        flexDirection: 'row',
    },
    button: {
        height: buttonHeight - 130,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: buttonWidth - 20,
        width: (screen.width / 4) - 10,
        margin: 5
    },
    buttonExpandSm: {
        height: buttonHeight - 130,
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        borderRadius: buttonWidth - 20,
        width: (screen.width / 2) - 10,
        margin: 5,
        flex: 0,
        paddingLeft: 40,
        paddingTop: 20
    },
    buttonPrimary: {
        backgroundColor: '#a6a6a6',
    },
    buttonOperator: {
        backgroundColor: '#f09a36'
    },
    buttonNumber: {
        backgroundColor: '#333333'
    },
    textSecondary: {
        color: '#060606',
        fontSize: 35
    },
    text: {
        color: '#ffffff',
        fontSize: 35
    },
    textExpandSm: {
        color: '#ffffff',
        fontSize: 45
    },
    textResult: {
        color: '#ffffff',
        fontSize: 100
    },
    result: {
        margin: 15,
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'flex-end'
    }
  });