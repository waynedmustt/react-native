import React, { Component } from 'react';
import { combineReducers } from 'redux';
import { getCalculateResult } from '../reducer';
import RootProvider from '../../../components/provider';
import CalculatorPage from '../view';

const reducer = combineReducers({
    calculateResult: getCalculateResult,
});
export default class CalculatorPageProvider extends Component {
    render() {
        return (
            <RootProvider
                rootReducers={reducer}
            >
                <CalculatorPage />
            </RootProvider>
        );
    }
}