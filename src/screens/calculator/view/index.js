import React, { Component } from 'react';
import {
    TouchableOpacity,
    Text,
    View
} from 'react-native';
import styles from '../../../styles';
import { connect } from 'react-redux';
import {
 calculateData,
 startCalculateCall,
 endCalculateCall
} from '../../../actions/calculator';

class CalculatorPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentValue: '0',
            operator: null,
            previousValue: '0'
        };
    }

    handleCalculation(type, input) {
        let result;
        let {dispatch} = this.props;
        const startCalculateCallAction = startCalculateCall;
        const endCalculateCallAction = endCalculateCall;
        dispatch(startCalculateCallAction({type: type, input: input}));
        this.setState((state) => {
            result = this.props.calculateData(type, input, state)
        });
        dispatch(endCalculateCallAction(this.state));
    }

    componentDidMount() {
        this.handleCalculation('init');
    }

    render() {
        let {calculateResult} = this.props;
        return(
            <View style={styles.container}>
                <View style={styles.result}>
                    <Text style={styles.textResult}>{calculateResult.response.currentValue}</Text>
                </View>
                <View style={styles.buttonContainer}>
                    <TouchableOpacity
                    style={[styles.button, styles.buttonPrimary]}
                    onPress={() => this.handleCalculation('init')}
                    >
                        <Text style={styles.textSecondary}>{calculateResult.response.currentValue !== '0' ? 'C' : 'AC'}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                    style={[styles.button, styles.buttonPrimary]}
                    onPress={() => this.handleCalculation('number', '+/-')}
                    >
                        <Text style={styles.textSecondary}>+/-</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                    style={[styles.button, styles.buttonPrimary]}
                    onPress={() => this.handleCalculation('operator', '%')}
                    >
                        <Text style={styles.textSecondary}>%</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                    style={[styles.button, styles.buttonOperator]}
                    onPress={() => this.handleCalculation('operator', '/')}
                    >
                        <Text style={styles.text}>/</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.buttonContainer}>
                    <TouchableOpacity
                    style={[styles.button, styles.buttonNumber]}
                    onPress={() => this.handleCalculation('number', '7')}
                    >
                        <Text style={styles.text}>7</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                    style={[styles.button, styles.buttonNumber]}
                    onPress={() => this.handleCalculation('number', '8')}
                    >
                        <Text style={styles.text}>8</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                    style={[styles.button, styles.buttonNumber]}
                    onPress={() => this.handleCalculation('number', '9')}
                    >
                        <Text style={styles.text}>9</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                    style={[styles.button, styles.buttonOperator]}
                    onPress={() => this.handleCalculation('operator', 'x')}
                    >
                        <Text style={styles.text}>x</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.buttonContainer}>
                    <TouchableOpacity
                    style={[styles.button, styles.buttonNumber]}
                    onPress={() => this.handleCalculation('number', '4')}
                    >
                        <Text style={styles.text}>4</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                    style={[styles.button, styles.buttonNumber]}
                    onPress={() => this.handleCalculation('number', '5')}
                    >
                        <Text style={styles.text}>5</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                    style={[styles.button, styles.buttonNumber]}
                    onPress={() => this.handleCalculation('number', '6')}
                    >
                        <Text style={styles.text}>6</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                    style={[styles.button, styles.buttonOperator]}
                    onPress={() => this.handleCalculation('operator', '-')}
                    >
                        <Text style={styles.text}>-</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.buttonContainer}>
                    <TouchableOpacity
                    style={[styles.button, styles.buttonNumber]}
                    onPress={() => this.handleCalculation('number', '1')}
                    >
                        <Text style={styles.text}>1</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                    style={[styles.button, styles.buttonNumber]}
                    onPress={() => this.handleCalculation('number', '2')}
                    >
                        <Text style={styles.text}>2</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                    style={[styles.button, styles.buttonNumber]}
                    onPress={() => this.handleCalculation('number', '3')}
                    >
                        <Text style={styles.text}>3</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                    style={[styles.button, styles.buttonOperator]}
                    onPress={() => this.handleCalculation('operator', '+')}
                    >
                        <Text style={styles.text}>+</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.buttonContainer}>
                    <TouchableOpacity
                    style={[styles.buttonExpandSm, styles.buttonNumber]}
                    onPress={() => this.handleCalculation('number', '0')}
                    >
                        <Text style={styles.textExpandSm}>0</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                    style={[styles.button, styles.buttonNumber]}
                    onPress={() => this.handleCalculation('number', ',')}
                    >
                        <Text style={styles.text}>,</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                    style={[styles.button, styles.buttonOperator]}
                    onPress={() => this.handleCalculation('result')}
                    >
                        <Text style={styles.text}>=</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        calculateData: (type, result, state) => calculateData(type, result, state),
        dispatch
    }
};

export default connect(
    (state) => {return state}, mapDispatchToProps)(CalculatorPage);