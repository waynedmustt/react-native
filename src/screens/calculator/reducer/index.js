import {
    START_CALCULATE,
    END_CALCULATE
} from '../../../types';

export function getCalculateResult(state={request: [], response: []},action) {
    switch (action.type) {
        case START_CALCULATE:
            return {
                request: action.request
            }
        case END_CALCULATE:
            return {
                response: action.response
            }
    }
    return state;
}