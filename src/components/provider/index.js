import React, { Component } from 'react';
import { applyMiddleware, createStore, compose } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';

let logger = createLogger({
    timestamps: true,
    duration: true
  });

class RootProvider extends Component {
    constructor(props) {
        super(props);
        this.rootStore = createStore(
        this.props.rootReducers,
        compose(applyMiddleware(thunk, logger))
        );
    }

    render() {
        return (
            <Provider store={this.rootStore}>
                {this.props.children}
            </Provider>
        );
    }
}

export default RootProvider;