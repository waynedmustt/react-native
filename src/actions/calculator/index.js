import {
    START_CALCULATE,
    END_CALCULATE
} from '../../types';

export function startCalculateCall(request) {
    return {
        type: START_CALCULATE,
        request
    }
}

export function endCalculateCall(response) {
    return {
        type: END_CALCULATE,
        response
    }
}

function result(state) {
    switch(state.operator) {
        case '+':
            return parseInt(state.currentValue) + parseInt(state.previousValue);
        case '-':
            return parseInt(state.previousValue) - parseInt(state.currentValue);
        case 'x':
            return parseInt(state.currentValue) * parseInt(state.previousValue);
        case '/':
            return parseInt(state.currentValue) / parseInt(state.previousValue);
        case '%':
            return parseInt(state.currentValue) / 100;
    }
}

export function calculateData(type, input, state) {
    switch (type) {
        case 'init':
            state.previousValue = '0';
            state.currentValue = '0';
            state.operator = null;
            break;
        case 'number':
            if (state.operator) {
                state.previousValue = state.currentValue;
                if (input === '+/-') {
                    state.currentValue = state.currentValue.indexOf('-') !== -1 ? `${state.currentValue.replace('-', '')}`: `${'-'}${state.currentValue}`;
                    return;
                }
                state.currentValue = `${input}`;
                return;
            }
            state.previousValue = state.currentValue;
            if (input === '+/-') {
                state.currentValue = state.currentValue.indexOf('-') !== -1 ? `${state.currentValue.replace('-', '')}`: `${'-'}${state.currentValue}`;
                return;
            }
            if (state.currentValue !== '0' && state.currentValue !== '-0') {
                state.currentValue = `${state.currentValue}${input}`;
            } else {
                state.currentValue = `${input}`;
            }
            break;
        case 'operator':
            state.operator = input;
            if (state.operator === '%') {
                state.currentValue = `${result(state)}`;
                state.previousValue = state.currentValue;
            }
            break;
        case 'result':
            if (!state.operator) {
                return;
            }

            state.currentValue = `${result(state)}`;
            state.previousValue = state.currentValue;
            break;
        default:
            state.previousValue = '0';
            state.currentValue = '0';
            break;
    }
}