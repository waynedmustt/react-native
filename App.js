import React from 'react';
import CalculatePageProvider from './src/screens/calculator/provider';

export default function App() {
  return (
    <CalculatePageProvider />
  );
}
